import React from 'react'

import IconWiki from '../../assets/images/icon-wiki.png'
import IconLike from '../../assets/images/icon-like.png'
import IconDislike from '../../assets/images/icon-dislike.png'

import './scss/info.scss'

function Info(){
    return (
        <article className="main-info">
            <div className="main-info__overlay">
                <p className="main-info__question">What's your opinion on</p>
                <h2 className="main-info__person-name">Pope Francis?</h2>
                <p className="main-info__person-description">He's talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)</p>
                <p className="main-info__more-information"><img src={IconWiki} alt="more information"/> <a href="#">More information</a></p>
                <p className="main-info__person-action">What's your veredict</p>
            </div>
            <div className="content-icons">
                <div className="content-icons__icon content-icons__icon--like">
                    <img src={IconLike} className="content-icons__image" />
                </div>
                <div className="content-icons__icon content-icons__icon--dislike">
                    <img src={IconDislike} className="content-icons__image"/>
                </div>
            </div>
        </article>
    )
}   

export default Info