import React, { Fragment } from 'react';
import { StoreProvider } from 'easy-peasy';
import Store from './store';

import MainHeader from './components/header';
import VotingList from './components/voting-list';
import MainFooter from './components/footer';

import './App.scss';

function App() {
  return (
    <StoreProvider store={Store}>
      <MainHeader />
      <VotingList />
      <MainFooter />
    </StoreProvider>
  );
}

export default App;
