import React from 'react'

import IconSearch from '../../assets/images/icon-search.png'
import './scss/index.scss'

function NavBar(){
    return (
        <nav className="main-navbar">
            <div className="fluid-container">
                <h1 className="main-navbar__title">Rule of Thumb.</h1>

                <ul className="main-navbar__list">
                    <a href="#" className="main-navbar__item">
                        <li>
                            Past Trials
                        </li>
                    </a>
                    <a href="#" className="main-navbar__item">
                        <li>
                            How Its Works
                        </li>
                    </a>
                    <a href="#" className="main-navbar__item">
                        <li>
                            Log In / Sign Up
                        </li>
                    </a>

                    <a href="#" className="main-navbar__item">
                        <li className="main-navbar__item main-navbar__item--search">
                            <img src={IconSearch} alt="search" />
                        </li>
                    </a>
                </ul>
            </div>
        </nav>
    )
}   

export default NavBar