import React from 'react';
import TestRenderer from 'react-test-renderer';
import { StoreProvider } from 'easy-peasy'

import Store from '../../store'
import Item from './Item';

describe('<Item />', () => {
    it('Show icon next to title', () => {
        let data = {
            id: 1,
            likes: 0,
            dislikes: 0,
            voted: false
        }

        let component = TestRenderer.create(
            <StoreProvider store={Store}>
                <Item data={data}/>
            </StoreProvider>
        );

        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--like')).toBe(false);
        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--dislike')).toBe(false);

        data = {
            id: 1,
            likes: 3,
            dislikes: 0,
            voted: false
        }

        component = TestRenderer.create(
            <StoreProvider store={Store}>
                <Item data={data}/>
            </StoreProvider>
        );
    
        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--like')).toBe(true);
        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--dislike')).toBe(false);

        data = {
            id: 1,
            likes: 2,
            dislikes: 3,
            voted: false
        }

        component = TestRenderer.create(
            <StoreProvider store={Store}>
                <Item data={data}/>
            </StoreProvider>
        );

        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--like')).toBe(false);
        expect(component.toJSON().children[0].props.className.includes('item-list__name-with-trend--dislike')).toBe(true);
    });
});