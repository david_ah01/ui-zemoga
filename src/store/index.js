import { createStore, action } from 'easy-peasy'
import Data from '../data/candidates'

const store = createStore({
    elections: {
        candidates: JSON.parse(localStorage.getItem('data')) || Data,
        addVote: action((state, payload) => {
            const candidate = state.candidates.filter(candidate => candidate.id === payload.id)[0]
            candidate.voted = payload.type;

            if(payload.type === 'like'){
                candidate.likes +=1
            }else{
                candidate.dislikes +=1
            }

            localStorage.setItem('data', JSON.stringify(state.candidates))
        }),
        removeVote: action((state, payload) => {
            const candidate = state.candidates.filter(candidate => candidate.id === payload.id)[0]
            candidate.voted = null;

            localStorage.setItem('data', JSON.stringify(state.candidates))
        })
    }
});

export default store