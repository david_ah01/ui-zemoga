import React from 'react'

import IconFb from '../../assets/images/icon-fb.png'
import IconTwitter from '../../assets/images/icon-twitter.png'
import './scss/index.scss'

function MainFooter(){
    return (
        <footer className="main-footer">
            <div className="fluid-container">
                <ul className="main-footer-links">
                    <a href="#" className="main-footer-links__link">
                        <li>Terms and Conditions</li>
                    </a>
                    <a href="#" className="main-footer-links__link">
                        <li>Privacy Policy</li>
                    </a>
                    <a href="#" className="main-footer-links__link">
                        <li>Contact Us</li>
                    </a>
                </ul>

                <ul className="main-footer-networks">
                    <li className="main-footer-networks__title">Follow Us</li>
                    <a href="#" className="main-footer-networks__link">
                        <li> <img src={IconFb} alt="Facebook" /> </li>
                    </a>
                    <a href="#" className="main-footer-networks__link">
                        <li> <img src={IconTwitter}  alt="Twitter" /> </li>
                    </a>
                </ul>
            </div>
        </footer>
    )
}

export default MainFooter