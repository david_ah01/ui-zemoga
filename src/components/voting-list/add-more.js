import React from 'react'

import './scss/add-more.scss'

function AddMore(){
    return (
        <div className="add-more">
            <div className="add-more__description">
                <p>Is there anyone else you would want us to add?</p>
            </div>

            <button type="button" className="add-more__button">
                Submit a name
            </button>
        </div>
    )
}

export default AddMore