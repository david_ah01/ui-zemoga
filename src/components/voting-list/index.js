import React from 'react'
import { useStoreState } from 'easy-peasy'

import Invitation from './invitation'
import Item from './item'
import AddMore from './add-more'
import './scss/index.scss'

function VotingList(){
    const candidates = useStoreState(state => state.elections.candidates)

    const renderItems = () => {
        return candidates.map(item => {
            return <Item key={`item-${item.id}`} data={item} />
        })
    }

    return (
        <div className="voting-section fluid-container">
            <Invitation />

            <h2 className="voting-section__title">Votes</h2>
            
            <div className="voting-grid">
                { renderItems() }
            </div>

            <AddMore />
        </div>
    )
}

export default VotingList