import React from 'react'

import IconLike from './../../../assets/images/icon-like.png'
import IconDislike from './../../../assets/images/icon-dislike.png'
import './scss/trend-bar.scss'

function TrendBar({ likes, dislikes }){

    const calcPorcentage = (base) => {
        const totalVotes = likes + dislikes
        if(totalVotes === 0){
            return '0%';
        }

        return `${(base * 100 / (likes + dislikes)).toFixed(1).replace('.0', '')}%`;
    }

    return (
        <div className="trend-bar">
            <div className="trend-bar__progress trend-bar__progress--like" style={{ width: `${calcPorcentage(likes)}` }}></div>
            <div className="trend-bar__progress trend-bar__progress--dislike" style={{ width: `${calcPorcentage(dislikes)}` }}></div>
            
            <div className="trend-bar__data trend-bar__data--like">
                <img src={IconLike} alt="like" />
                {calcPorcentage(likes)}
            </div>
            <div className="trend-bar__data trend-bar__data--dislike">
                <img src={IconDislike} alt="dislike" />
                {calcPorcentage(dislikes)}
            </div>
        </div>
    )
}

TrendBar.defaultProps = {
    like: 0,
    dislikes: 0
}

export default TrendBar