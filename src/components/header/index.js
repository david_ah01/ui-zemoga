import React from 'react'
import NavBar from '../navbar'
import Info from './info'
import './scss/index.scss'

function MainHeader(){
    return (
        <header className="main-header">
            <NavBar />

            <div className="fluid-container">
                <Info />
            </div>

            <aside className="closing-data">
                <div className="closing-data__title">
                    Closing in
                </div>
                <div className="closing-data__days">
                    <p><span>22</span> days</p>
                </div>
            </aside>
        </header>
    )
}   

export default MainHeader