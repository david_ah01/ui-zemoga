import React from 'react';
import { shallow } from '../../../enzyme'

import VotingBox from './voting-box';

describe('<VotingBox />', () => {
    it('Toggle active className on select option', () => {
        const addVote = jest.fn();
        const removeVote = jest.fn();

        const data = {
            id: 1,
            name: "Kanye West",
            picture: '',
            likes: 0,
            dislikes: 0,
            voted: false,
            date: '1 month ago',
            category: 'Entertaiment',
            description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
        }

        const component = shallow((
            <VotingBox data={data} addVote={addVote} removeVote={removeVote} />
        ));

        component.find('.voting-box__option--like').simulate('click');
        expect(component.find('.voting-box__option--active')).toHaveLength(1);

        component.find('.voting-box__option--dislike').simulate('click');
        expect(component.find('.voting-box__option--active')).toHaveLength(1);
    });

    it('Simulate add like vote click', () => {
        const addVote = jest.fn();
        const removeVote = jest.fn();

        const data = {
            id: 1,
            name: "Kanye West",
            picture: '',
            likes: 0,
            dislikes: 0,
            voted: false,
            date: '1 month ago',
            category: 'Entertaiment',
            description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
        }

        const component = shallow((
            <VotingBox data={data} addVote={addVote} removeVote={removeVote} />
        ));

        component.find('.voting-box__option--like').simulate('click');
        component.find('button').simulate('click');
        expect(addVote.mock.calls.length).toEqual(1);
        expect(addVote.mock.calls[0][0]).toEqual({ id: 1, type: 'like' });
    });

    it('Simulate add dislike vote click', () => {
        const addVote = jest.fn();
        const removeVote = jest.fn();

        const data = {
            id: 1,
            name: "Kanye West",
            picture: '',
            likes: 0,
            dislikes: 0,
            voted: false,
            date: '1 month ago',
            category: 'Entertaiment',
            description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
        }

        const component = shallow((
            <VotingBox data={data} addVote={addVote} removeVote={removeVote} />
        ));

        component.find('.voting-box__option--dislike').simulate('click');
        component.find('button').simulate('click');
        expect(addVote.mock.calls.length).toEqual(1);
        expect(addVote.mock.calls[0][0]).toEqual({ id: 1, type: 'dislike' });
    });

    it('Simulate remove vote click', () => {
        const addVote = jest.fn();
        const removeVote = jest.fn();

        const data = {
            id: 1,
            name: "Kanye West",
            picture: '',
            likes: 0,
            dislikes: 0,
            voted: 'like',
            date: '1 month ago',
            category: 'Entertaiment',
            description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
        }

        const component = shallow((
            <VotingBox data={data} addVote={addVote} removeVote={removeVote} />
        ));

        component.find('button').simulate('click');
        expect(removeVote.mock.calls.length).toEqual(1);
        expect(removeVote.mock.calls[0][0]).toEqual({ id: 1 });
    });
});