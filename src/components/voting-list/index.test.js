import React from 'react';
import TestRenderer from 'react-test-renderer';
import { StoreProvider } from 'easy-peasy'

import Store from '../../store'
import VotingList from './index';

describe('<VotingList />', () => {
  it('renders four <Item /> components', () => {
    const app = TestRenderer.create(
      <StoreProvider store={Store}>
        <VotingList />
      </StoreProvider>
    )

    const { children } = app.toJSON();
    expect(children[2].props.className).toBe('voting-grid');
    expect(children[2].children).toHaveLength(4);
    expect(children[2].children[0].props.className).toBe('item-list');
    expect(children[2].children[1].props.className).toBe('item-list');
    expect(children[2].children[2].props.className).toBe('item-list');
    expect(children[2].children[3].props.className).toBe('item-list');
  });
});