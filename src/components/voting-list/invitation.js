import React from 'react'

import IconClose from '../../assets/images/icon-close.png'

import './scss/invitation.scss'

function Invitation(){
    return (
        <div className="invitation">
            <div className="invitation__title">
                <p>Speak out. Be heard.</p>
                <strong>Be counted</strong>
            </div>

            <div className="invitation__description">
                Rule of Thumb is a crowd sourced court of public opinion where anyone and everyone can speak out and speak freely. It’s easy: You share your opinion, we analyze and put the data in a public report.
            </div>

            <img src={IconClose} className="invitation__close" alt="close" />
        </div>
    )
}

export default Invitation