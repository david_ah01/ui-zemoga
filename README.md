## Hi Everybody

This is a project made with React. To run it just put on the terminal **npm start**

I used *easy peasy* as state handler and *pure css* (scss and flexbox) without frameworks, for the web page design.

I made tests for the components with interactions. To run it just put on the terminal **npm test**