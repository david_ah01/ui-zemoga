import React from 'react'
import { useStoreActions } from 'easy-peasy'

import VotingBox from './components/voting-box'
import TrendBar from './components/trend-bar'
import './scss/item.scss'

function ItemList({ data }){
    const addVote = useStoreActions(actions => actions.elections.addVote);
    const removeVote = useStoreActions(actions => actions.elections.removeVote);

    return (
        <div className="item-list" style={{ backgroundImage: `url('${data.picture}')` }}>
            <h3 className={`item-list__name item-list__name-with-trend 
                            ${data.likes > data.dislikes ? 'item-list__name-with-trend--like' : ''}
                            ${data.dislikes > data.likes ? 'item-list__name-with-trend--dislike' : ''}`}>
                { data.name }
            </h3>
            <p className="item-list__date">
                <strong>{ data.date }</strong> in { data.category }
            </p>
            { !data.voted ?
                <p className="item-list__description">
                    { data.description }
                </p> :
                    <p className="item-list__thank-you">
                    Thank you for voting!
                </p>
            }

            <VotingBox data={data} addVote={addVote} removeVote={removeVote} />

            <TrendBar likes={data.likes} dislikes={data.dislikes} />
        </div>
    )
}

export default ItemList