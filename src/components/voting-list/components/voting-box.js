import React, { Fragment, useState } from 'react'

import IconLike from './../../../assets/images/icon-like.png'
import IconDislike from './../../../assets/images/icon-dislike.png'
import './scss/voting-box.scss'

function VotingBox({ data, addVote, removeVote }){
    const [option, setOption] = useState()
    
    const toggleOption = (clickedOption) => {
        if(clickedOption === option){
            setOption()
        }else{
            setOption(clickedOption)
        }
    }

    return (
        <div className="voting-box">
            { !data.voted && <Fragment>
                <div 
                    className={`voting-box__option voting-box__option--like ${option === 'like' ? 'voting-box__option--active' : ''}`}
                    onClick={() => toggleOption('like')}></div>
                <div 
                    className={`voting-box__option voting-box__option--dislike ${option === 'dislike' ? 'voting-box__option--active' : ''}`}
                    onClick={() => toggleOption('dislike')}></div>

                <button type="button" 
                        disabled={!option} 
                        className="voting-box__button" 
                        onClick={() => addVote({ id: data.id, type: option })}>Vote now</button>
            </Fragment> }

            { data.voted && <Fragment>
                <button type="button" 
                        className="voting-box__button" 
                        onClick={() => {removeVote({ id: data.id }); setOption()}}>Vote again</button>
            </Fragment> }
        </div>
    )
}

VotingBox.defaultProps = {
    like: 0,
    dislikes: 0
}

export default VotingBox