import React from 'react';
import { shallow } from '../../../enzyme'

import TrendBar from './trend-bar';

describe('<TrendBar />', () => {
    it('Validate percentage calculation', () => {
        let component = shallow((
            <TrendBar likes={4} dislikes={4} />
        ));

        expect(component.find('.trend-bar__data--like').text()).toEqual(
            expect.stringMatching('50%')
        )
        
        expect(component.find('.trend-bar__data--dislike').text()).toEqual(
            expect.stringMatching('50%')
        )

        component = shallow((
            <TrendBar likes={1} dislikes={4} />
        ));
        
        expect(component.find('.trend-bar__data--like').text()).toEqual(
            expect.stringMatching('20%')
        )
        
        expect(component.find('.trend-bar__data--dislike').text()).toEqual(
            expect.stringMatching('80%')
        )
    });

    it('Validate bar size', () => {
        let component = shallow((
            <TrendBar likes={4} dislikes={4} />
        ));

        expect(component.find('.trend-bar__progress--like').prop('style')).toHaveProperty('width', '50%');
        expect(component.find('.trend-bar__progress--dislike').prop('style')).toHaveProperty('width', '50%');

        component = shallow((
            <TrendBar likes={2} dislikes={4} />
        ));

        expect(component.find('.trend-bar__progress--like').prop('style')).toHaveProperty('width', '33.3%');
        expect(component.find('.trend-bar__progress--dislike').prop('style')).toHaveProperty('width', '66.7%');
    });

});