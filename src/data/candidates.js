import PictureKanye from '../assets/images/picture-kanye.png'
import PictureMark from '../assets/images/picture-mark.png'
import PictureCristina from '../assets/images/picture-cristina.png'
import PictureMalala from '../assets/images/picture-malala.png'

export default [
    {
        id: 1,
        name: "Kanye West",
        picture: PictureKanye,
        likes: 0,
        dislikes: 0,
        voted: false,
        date: '1 month ago',
        category: 'Entertaiment',
        description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
    },
    {
        id: 2,
        name: "Mark Zuckerberg",
        picture: PictureMark,
        likes: 0,
        dislikes: 0,
        voted: false,
        date: '1 month ago',
        category: 'Business',
        description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
    },
    {
        id: 3,
        name: "Cristina Fernández de Kirchner",
        picture: PictureCristina,
        likes: 0,
        dislikes: 0,
        voted: false,
        date: '1 month ago',
        category: 'Politics',
        description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
    },
    {
        id: 4,
        name: "Malala Yousafzai",
        picture: PictureMalala,
        likes: 0,
        dislikes: 0,
        voted: false,
        date: '1 month ago',
        category: 'Entertaiment',
        description: "Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero"
    }
]